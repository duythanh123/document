-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 27, 2019 lúc 04:27 AM
-- Phiên bản máy phục vụ: 10.1.34-MariaDB
-- Phiên bản PHP: 7.0.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `shopping`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `quatity` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `cart`
--

INSERT INTO `cart` (`cart_id`, `pro_id`, `quatity`, `order_id`, `customer_id`) VALUES
(2, 19, 1, 5, 4),
(3, 20, 1, 5, 4),
(4, 16, 1, 6, 4),
(5, 20, 3, 6, 4),
(6, 14, 1, 6, 4),
(7, 20, 1, 7, 4),
(8, 21, 1, 7, 4),
(9, 17, 3, 8, 4),
(10, 18, 10, 8, 4),
(11, 15, 3, 8, 4),
(12, 17, 1, 9, 4),
(13, 18, 1, 9, 4),
(14, 19, 2, 9, 4),
(15, 17, 2, 10, 4),
(16, 18, 1, 10, 4),
(17, 21, 1, 11, 4),
(18, 17, 3, 12, 4),
(19, 21, 1, 13, 4),
(20, 18, 1, 14, 4),
(22, 21, 2, 16, 4),
(23, 21, 1, 17, 4),
(24, 20, 1, 18, 4),
(25, 21, 1, 19, 4),
(26, 21, 1, 20, 4),
(27, 17, 2, 21, 4),
(28, 21, 1, 22, 4),
(29, 17, 2, 25, 4),
(30, 21, 1, 25, 4),
(31, 21, 1, 26, 4),
(32, 21, 1, 27, 4),
(36, 17, 1, 31, 4),
(37, 17, 1, 32, 4),
(47, 30, 2, 42, 4),
(50, 29, 1, 45, 4),
(51, 30, 2, 46, 4),
(65, 27, 1, 60, 4),
(66, 37, 1, 61, 4),
(67, 35, 1, 62, 4),
(68, 25, 1, 63, 4);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `cid` int(11) NOT NULL,
  `cname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `parrent_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `subject` text COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`id`, `name`, `username`, `password`, `email`, `active`) VALUES
(1, 'Trang Trần', 'trangttn', '123456', 'trang@gmai.com', 1),
(2, 'Trang Trần', 'trangttn11', '$2a$10$/sc4SYSUiAy9HjLVWALEH.r.jHKpUVOLJu31tB5HBCVp6s4kyu2WC', 'trangtran@gmail.com', 1),
(3, 'Trang Trần', 'user', '$2a$10$9MQfHEylA020f3Tjp.jRHOerOSlprCBNC26by8hpH6smz/p2E1LkC', 'tranginspirit78@gmail.com', 1),
(4, 'Trang Trần ', 'user3', '12345', 'trangtran@gmail.com', 1),
(5, 'Trang Trần', 'trangttn111', '12345', 'trangtran@gmail.com', 1),
(6, 'Trang Trần', 'admin', '12345', 'trangtran@gmail.com', 1),
(7, 'Trang Trần', 'admindemo', '12345', 'trangtran@gmail.com', 1),
(8, 'Trang Trần', 'editors', '12345', 'c@gmail.com', 1),
(9, 'Trang Trần', 'abc', '12345', 'tranginspirit78@gmail.com', 1),
(11, 'Trang Trần', 'user4', '12345', 'tranginspirit78@gmail.com', 1),
(12, 'Trang Trần Ngoc', 'trangtranngoc', '11111', 'trangtran@gmail.com', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `giftcode`
--

CREATE TABLE `giftcode` (
  `gid` int(11) NOT NULL,
  `gname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `value` int(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `giftcode`
--

INSERT INTO `giftcode` (`gid`, `gname`, `amount`, `value`) VALUES
(1, 'PA4ZE3', 10, 50),
(2, 'DXDIAG', 12, 40),
(3, 'PAO20A', 3, 70),
(4, 'PAKZK2', 2, 80);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bill` bigint(11) NOT NULL,
  `payments` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `paid` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Chưa thanh toán',
  `giftcode` int(11) NOT NULL DEFAULT '0',
  `checked` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`order_id`, `name`, `phone`, `address`, `date`, `bill`, `payments`, `status`, `paid`, `giftcode`, `checked`) VALUES
(5, 'Trang Trần', '0378424500', 'Quận Tân Bình - TP.HCM', '2019-05-25 14:15:46', 0, 'Giao hàng tại nhà', 'Đang chờ', 'Đã thanh toán', 0, 1),
(6, 'Trang', '0378424500', 'Đà Nẵng', '2019-05-26 09:18:13', 0, 'Giao hàng tại nhà', 'Đã được gửi đi', 'Chưa thanh toán', 0, 0),
(7, 'Trang Trần', '0378424500', 'Da nang', '2019-05-26 10:06:51', 0, 'Giao hàng tại nhà', 'Đã được gửi đi', 'Chưa thanh toán', 0, 1),
(8, 'Trần Thị Ngân', '0378424500', 'Quận 1 - TP.HCM ', '2019-05-27 15:32:44', 0, 'Giao hàng tại nhà', 'Đang chờ', 'Chưa thanh toán', 0, 1),
(9, 'Phan Thanh Trúc', '0378424500', 'Quận Tân Bình - TP.HCM', '2019-05-27 15:37:50', 0, 'Giao hàng tại nhà', 'Đang chờ', 'Chưa thanh toán', 0, 0),
(10, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-03 17:46:24', 0, 'Giao hàng tại nhà', 'Đang chờ', 'Chưa thanh toán', 0, 1),
(11, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-02-03 17:55:56', 0, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 0, 1),
(12, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-03 18:10:36', 0, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 0, 0),
(13, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-03 18:12:07', 0, 'Thanh toán sau khi nhận hàng', 'Đang chờ', 'Chưa thanh toán', 0, 0),
(14, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2016-06-03 18:30:04', 110000, 'Thanh toán sau khi nhận hàng', 'Đang chờ', 'Chưa thanh toán', 0, 1),
(16, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 06:13:52', 0, 'Thanh toán sau khi nhận hàng', 'Đang chờ', 'Chưa thanh toán', 0, 0),
(17, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 06:34:02', 0, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 0, 0),
(18, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 06:34:42', 0, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 0, 0),
(19, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 06:36:03', 0, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 0, 0),
(20, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 06:38:02', 0, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 0, 0),
(21, 'Trang Trần Ngọc', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 06:41:36', 0, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 0, 0),
(22, 'Trang Trần Ngọc', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 06:42:04', 0, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 0, 0),
(23, 'Trần Thị Trang', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 08:10:52', 0, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 0, 0),
(24, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 08:16:41', 0, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 0, 1),
(25, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 08:21:44', 0, 'Thanh toán với paypal ', 'Hoàn tất', 'Đã thanh toán', 0, 1),
(26, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 08:24:35', 0, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 0, 0),
(27, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 08:27:17', 0, 'Thanh toán với paypal ', 'Hoàn tất', 'Đã thanh toán', 0, 1),
(31, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 08:54:25', 0, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 0, 1),
(32, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 08:56:14', 0, 'Thanh toán với paypal ', 'Hoàn tất', 'Đã thanh toán', 0, 1),
(36, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 09:14:59', 0, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 0, 1),
(42, 'Trang Trần', '0378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 12:26:51', 0, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 0, 1),
(45, 'Trang Trần', '378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 16:54:25', 300000, 'Thanh toán sau khi nhận hàng', 'Hoàn tất', 'Đã thanh toán', 50, 1),
(46, 'Trang Trần', '378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 16:57:32', 128000, 'Thanh toán sau khi nhận hàng', 'Hoàn tất', 'Đã thanh toán', 50, 1),
(60, 'Trang Trần', '378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-04 17:50:53', 120000, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 50, 1),
(61, 'Trang Trần', '378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-08 06:58:16', 2280000, 'Thanh toán sau khi nhận hàng', 'Hoàn tất', 'Đã thanh toán', 0, 1),
(62, 'Trang Trần Ngọc', '378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-08 07:00:05', 775000, 'Thanh toán sau khi nhận hàng', 'Hoàn tất', 'Đã thanh toán', 50, 1),
(63, 'Trang Trần', '378424500', 'k856/43 Ton Duc Thang, Hoa Khanh Bac', '2019-06-09 03:52:35', 390000, 'Thanh toán với paypal ', 'Đang chờ', 'Đã thanh toán', 0, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cid` int(11) NOT NULL,
  `price` int(50) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `detail` text COLLATE utf8_unicode_ci,
  `picture` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lcd` text COLLATE utf8_unicode_ci,
  `ram` text COLLATE utf8_unicode_ci,
  `hdd` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ssd` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vga` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pin` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `os` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cpu` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other` text COLLATE utf8_unicode_ci,
  `view` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: còn hàng , 0: hết hàng'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `roleId` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`roleId`, `name`) VALUES
(1, 'ADMIN'),
(3, 'MOD');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slide`
--

CREATE TABLE `slide` (
  `id` int(11) NOT NULL,
  `picture` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slide`
--

INSERT INTO `slide` (`id`, `picture`, `status`) VALUES
(13, '1480558760_banner-1-1467449427612051-1467449428410381.png', 1),
(14, '215623042496518-1467461629427186-1467461630433195.jpg', 1),
(15, 'bg2-1467469620912348-1467469621578565.jpg', 1),
(16, 'maxresdefault-1156728398615300-1156728404358200.jpg', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roleId` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: active, 0 : no active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `username`, `fullname`, `password`, `roleId`, `active`) VALUES
(4, 'admin', 'Trần Thị Ngọc Trang', '$2a$10$N1.yX/pgI2unkbV61X9oXuVMQrwHfdFmnvqjpavrs1EpD8xj8xyBe', 1, 1),
(5, 'user3', 'Trần Văn C', '$2a$10$HbeJr3g/IdjximFrvZi2YuR57XiGF3smaBtbz4eVDH9qIhnn48J.u', 3, 1),
(6, 'editor2', 'Lâm Ngọc Khương', '$2a$10$vh5IUK49/aXmZLdiEnGpDOGCTUNB7.yAIwZcNdUkBg3gvXI/XiDTq', 3, 1),
(7, 'trangttn11', 'VinaEnter Edu2', '$2a$10$Xg.0BEOmz2GHE6XOHKmQDOsR4UUcujiGw/zZx2oNeZbzgoycFTnkC', 3, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vnecontact`
--

CREATE TABLE `vnecontact` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `vnecontact`
--

INSERT INTO `vnecontact` (`id`, `name`, `phone`, `email`, `subject`, `message`) VALUES
(13, 'Trang Trần', '0378424500', 'trangtran@gmail.com', 'aaaa', 'ewe'),
(14, 'Trang Trần', '0378424500', 'trangtran@gmail.com', 'aaaa', '123'),
(15, 'Trang Trần', '0378424500', 'trangtran@gmail.com', 'aaaa', '1111');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cid`);

--
-- Chỉ mục cho bảng `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `giftcode`
--
ALTER TABLE `giftcode`
  ADD PRIMARY KEY (`gid`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`roleId`),
  ADD KEY `roleId` (`roleId`);

--
-- Chỉ mục cho bảng `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `vnecontact`
--
ALTER TABLE `vnecontact`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT cho bảng `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `giftcode`
--
ALTER TABLE `giftcode`
  MODIFY `gid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `roleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `vnecontact`
--
ALTER TABLE `vnecontact`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
